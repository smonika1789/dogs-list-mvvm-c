//
//  HomeViewModelTests.swift
//  DogsListMVVMTests
//
//  Created by Monika Sonawave on 12/07/2023.
//

import XCTest
@testable import DogsListMVVM

final class HomeViewModelTests: XCTestCase {

    var coordinator: AppCoordinatorMock!
    var service: ServicesMock!
    var viewModel: HomeViewModel!
    
    override func setUpWithError() throws {
        coordinator = AppCoordinatorMock()
        service = ServicesMock()
        viewModel = HomeViewModel(coordinator: coordinator, service: service)
    }
    
    override func tearDown() {
        viewModel = nil
        service = nil
        coordinator = nil
        super.tearDown()
    }
    
    override func tearDownWithError() throws {
        service = nil
        viewModel = nil
        coordinator = nil
    }
    
    func test_getDogList() {
        coordinator.start()
        let expectation = XCTestExpectation(description: "Test Dogs List")
        service.data = loadStub(name: "DogsResponse", extension: "json")

        viewModel.reloadCollectionView = {
            XCTAssertFalse(self.viewModel.dogs.count == 0)
            expectation.fulfill()
        }
        
        viewModel.getDogsList()
        wait(for: [expectation], timeout: 30.0)
    }
    
    func test_dogs_Details() {
        service.data = loadStub(name: "DogsResponse", extension: "json")
        let expectation = XCTestExpectation(description: "Test Dogs data")

        viewModel.reloadCollectionView = {
            XCTAssertEqual(3, self.viewModel.numberOfItems)
            XCTAssertEqual("Affenpinscher", self.viewModel.dogName(For: 0))
            XCTAssertEqual("https://cdn2.thedogapi.com/images/BJa4kxc4X.jpg", self.viewModel.dogImage(For: 0))
            expectation.fulfill()
        }
        viewModel.getDogsList()
        wait(for: [expectation], timeout: 30.0)
    }
}

extension XCTest {
    func loadStub(name: String, extension: String) -> Data {
        let bundle = Bundle(for: classForCoder)
        let url = bundle.url(forResource: name, withExtension: `extension`)
        
        return try! Data(contentsOf: url!)
    }
}
