//
//  ServicesMock.swift
//  DogsListMVVMTests
//
//  Created by Monika Sonawave on 12/07/2023.
//

@testable import DogsListMVVM
import Foundation

class ServicesMock: ServicesProtocol {
    var data: Data?
    var error: Error?
    
    func getlist(completion: @escaping DataCompletion)  {
        completion(data, error)
    }
}
