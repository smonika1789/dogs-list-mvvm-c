//
//  AppCoordinatorMock.swift
//  DogsListMVVMTests
//
//  Created by Monika Sonawave on 12/07/2023.
//

import UIKit
@testable import DogsListMVVM

class AppCoordinatorMock: AppCoordinatorProtocol {

    // MARK: - Properties
    var didGoToDetails = false

    var onDismiss: VoidCompletion?
    var childCoordinators: [Coordinator] = []
    var navigationController = UINavigationController()

    // MARK: - Coordination
    
    func goToHomeScreen() {}
    
    func goToDetails(details: Dogs) {
        didGoToDetails = true
    }
}
