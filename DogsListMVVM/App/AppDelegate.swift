//
//  AppDelegate.swift
//  DogsListMVVM
//
//  Created by Monika Sonawave on 10/07/2023.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    private var appCoordinator: AppCoordinator?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let window = UIWindow(frame: UIScreen.main.bounds)

        let appCoordinator = AppCoordinator(window: window)
        appCoordinator.start()

        self.window = window
        self.appCoordinator = appCoordinator

        return true
    }
}
