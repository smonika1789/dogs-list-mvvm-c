//
//  AppCoordinator.swift
//  DogsListMVVM
//
//  Created by Monika Sonawave on 10/07/2023.
//

import UIKit

protocol AppCoordinatorProtocol: Coordinator {
    func goToHomeScreen()
    func goToDetails(details: Dogs)
}

class AppCoordinator: AppCoordinatorProtocol {

    // MARK: - Properties

    var window: UIWindow

    var onDismiss: VoidCompletion?
    var childCoordinators: [Coordinator] = []
    var navigationController = UINavigationController()

    // MARK: - Initialization

    init(window: UIWindow) {
        self.window = window
    }

    // MARK: - Coordination
    
    func start() {
        goToHomeScreen()

        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }

    func goToHomeScreen() {
        let viewModel = HomeViewModel(coordinator: self)
        let destinationVC = HomeViewController(viewModel: viewModel)
        navigationController.setViewControllers([destinationVC], animated: true)
    }

    func goToDetails(details: Dogs) {
        let detailsViewController = DetailsViewController(details: details)
        navigationController.present(detailsViewController, animated: true)
    }
}
