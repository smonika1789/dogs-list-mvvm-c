//
//  HomeViewController.swift
//  DogsListMVVM
//
//  Created by Monika Sonawave on 10/07/2023.
//

import UIKit

class HomeViewController: UIViewController {
    
    // MARK: - Views
    
    private var viewModel: HomeViewModelProtocol
    
    private var layout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.itemSize = CGSize(width: Constants.collectionViewCellWidth, height: Constants.collectionViewCellHeight)
        layout.sectionInset = UIEdgeInsets(top: 0, left: Constants.commonSectionInset, bottom: Constants.commonSectionInset, right: Constants.commonSectionInset)
        return layout
    }()
    
    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.backgroundColor = .clear
        return collectionView
    }()
    
    // MARK: - Initialization
    
    init(viewModel: HomeViewModelProtocol) {
        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = Constants.pageTitle
        view.backgroundColor = .white
        
        setUpViews()
        setupConstraints()
        setupViewModel()
    }
    
    // MARK: - UI
    
    private func setUpViews() {
        collectionView.register(HomeCollectionViewCell.self)
        collectionView.delegate = self
        collectionView.dataSource = self
        view.addSubview(collectionView)
    }
    
    private func setupConstraints() {
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
    }
    
    //    MARK: - View Model
    
    func setupViewModel() {
        viewModel.reloadCollectionView = { [weak self] in
            DispatchQueue.main.async {
                self?.collectionView.reloadData()
            }
        }
        
        viewModel.getDogsList()        
    }
}

// MARK: - Collection View Delegate & Datasource

extension HomeViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfItems
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: HomeCollectionViewCell = collectionView.reuse(at: indexPath)
        cell.configure(
            title: viewModel.dogName(For: indexPath.row),
            imageName: viewModel.dogImage(For: indexPath.row))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: false)
        viewModel.navigateToDetails(index: indexPath.row)
    }
}

extension HomeViewController {
    struct Constants {
        static let collectionViewCellHeight: CGFloat = 175
        static let collectionViewCellWidth: CGFloat = UIScreen.main.bounds.width/2 - 10
        static let commonSectionInset: CGFloat = 5
        
        static let pageTitle: String = "Dogs List"
    }
}
