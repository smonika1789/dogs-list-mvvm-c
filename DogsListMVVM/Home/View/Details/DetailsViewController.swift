//
//  DetailsViewController.swift
//  DogsListMVVM
//
//  Created by Monika Sonawave on 12/07/2023.
//

import UIKit

class DetailsViewController: UIViewController {
    
    // MARK: - Views
    
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let titleLabelText: UILabel = {
        let label = UILabel()
        label.text = Constants.titleLabelText
        label.textColor = .black
        label.font = .systemFont(ofSize: Constants.fontSize)
        label.textAlignment = .left
        label.numberOfLines = 0
        return label
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = .systemFont(ofSize: Constants.fontSize)
        label.textAlignment = .left
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var titleStackView = UIStackView(
        arrangedSubviews: [titleLabelText,
                           titleLabel]
    )
    
    let originTitleLabel: UILabel = {
        let label = UILabel()
        label.text = Constants.originTitleLabel
        label.textColor = .black
        label.font = .systemFont(ofSize: Constants.fontSize)
        label.textAlignment = .left
        label.numberOfLines = 0
        return label
    }()
    
    let originLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = .systemFont(ofSize: Constants.fontSize)
        label.textAlignment = .left
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var originStackView = UIStackView(
        arrangedSubviews: [originTitleLabel,
                           originLabel]
    )
    
    let lifeSpanTitleLabel: UILabel = {
        let label = UILabel()
        label.text = Constants.lifeSpanTitleLabel
        label.textColor = .black
        label.font = .systemFont(ofSize: Constants.fontSize)
        label.textAlignment = .left
        label.numberOfLines = 0
        return label
    }()
    
    let lifeSpanLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = .systemFont(ofSize: Constants.fontSize)
        label.textAlignment = .left
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var lifeSpanStackView = UIStackView(
        arrangedSubviews: [lifeSpanTitleLabel,
                           lifeSpanLabel]
    )
    
    let temperamentTitleLabel: UILabel = {
        let label = UILabel()
        label.text = Constants.temperamentTitleLabel
        label.textColor = .black
        label.textAlignment = .left
        label.font = .systemFont(ofSize: Constants.fontSize)
        return label
    }()
    
    let temperamentLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = .systemFont(ofSize: Constants.fontSize)
        label.textAlignment = .left
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var temperamentStackView = UIStackView(
        arrangedSubviews: [temperamentTitleLabel,
                           temperamentLabel]
    )
    
    private lazy var detailsStackView = UIStackView(
        arrangedSubviews: [imageView,
                           titleStackView,
                           originStackView,
                           lifeSpanStackView,
                           temperamentStackView]
    )
    
    var dogDetails: Dogs?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        self.view.translatesAutoresizingMaskIntoConstraints = false
        setUpViews()
        setUpContraints()
        configureView()
    }
    
    init(details: Dogs?) {
        self.dogDetails = details
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - UI
    
    private func setUpViews() {
        detailsStackView.axis = .vertical;
        detailsStackView.distribution = .equalSpacing
        detailsStackView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(detailsStackView)
    }
    
    private func setUpContraints() {
        NSLayoutConstraint.activate([
            imageView.heightAnchor.constraint(equalToConstant: Constants.imageViewHeight),
            imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor),

            titleStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Constants.stackViewLeadingTrailing),
            titleStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Constants.stackViewLeadingTrailing),
            titleStackView.heightAnchor.constraint(equalToConstant: Constants.stackViewHeight),

            originStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Constants.stackViewLeadingTrailing),
            originStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Constants.stackViewLeadingTrailing),
            originStackView.heightAnchor.constraint(equalToConstant: Constants.stackViewHeight),

            lifeSpanStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Constants.stackViewLeadingTrailing),
            lifeSpanStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Constants.stackViewLeadingTrailing),
            lifeSpanStackView.heightAnchor.constraint(equalToConstant: Constants.stackViewHeight),

            temperamentStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Constants.stackViewLeadingTrailing),
            temperamentStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Constants.stackViewLeadingTrailing),
            temperamentStackView.heightAnchor.constraint(equalToConstant: Constants.stackViewHeight),

            detailsStackView.topAnchor.constraint(equalTo: imageView.safeAreaLayoutGuide.bottomAnchor),
        ])
    }
    
    func configureView() {
        titleLabel.text = dogDetails?.name
        originLabel.text = (dogDetails?.origin == "" || dogDetails?.origin == nil) ? "N/A" : dogDetails?.origin
        lifeSpanLabel.text = dogDetails?.life_span
        temperamentLabel.text = dogDetails?.temperament
        imageView.sd_setImage(with: URL(string: dogDetails?.image?.url ?? ""), placeholderImage: UIImage(named: "placeholder"))
    }
}

extension DetailsViewController {
    private struct Constants {
        static let imageViewHeight: CGFloat = 250
        static let stackViewHeight: CGFloat = 50
        static let stackViewLeadingTrailing: CGFloat = 25
        static let fontSize: CGFloat = 14
        
        static let titleLabelText = "Name : "
        static let originTitleLabel = "Origin : "
        static let lifeSpanTitleLabel = "Life Span : "
        static let temperamentTitleLabel = "Temperament : "
    }
}
