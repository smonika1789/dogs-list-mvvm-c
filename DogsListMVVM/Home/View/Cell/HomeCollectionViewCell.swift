//
//  HomeCollectionViewCell.swift
//  DogsListMVVM
//
//  Created by Monika Sonawave on 10/07/2023.
//

import UIKit
import SDWebImage

class HomeCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Views
    
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .red
        label.font = .systemFont(ofSize: 12)
        label.textAlignment = .center
        return label
    }()
    
    private lazy var dogDataStackView = UIStackView(
        arrangedSubviews: [imageView, titleLabel]
    )
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setUpViews()
        setUpContraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - UI
    
    private func setUpViews() {
        layer.borderWidth = Constants.borderWidth
        layer.borderColor = UIColor.darkGray.cgColor
        self.layer.cornerRadius = Constants.cornerRadius
        
        dogDataStackView.axis = .vertical;
        dogDataStackView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(dogDataStackView)
    }
    
    private func setUpContraints() {
        NSLayoutConstraint.activate([
            imageView.heightAnchor.constraint(equalToConstant: Constants.imageViewHeight),
            imageView.leadingAnchor.constraint(equalTo: leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            titleLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            dogDataStackView.topAnchor.constraint(equalTo: topAnchor),
            dogDataStackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            dogDataStackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            dogDataStackView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    func configure(title: String, imageName: String) {
        titleLabel.text = title
        imageView.sd_setImage(with: URL(string: imageName), placeholderImage: UIImage(named: "placeholder"))
    }
}

extension HomeCollectionViewCell {
    private struct Constants {
        static let imageViewHeight: CGFloat = 150
        static let borderWidth: CGFloat = 1
        static let cornerRadius: CGFloat = 5
    }
}
