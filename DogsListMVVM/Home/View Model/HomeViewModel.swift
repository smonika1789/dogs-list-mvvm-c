//
//  HomeViewModel.swift
//  DogsListMVVM
//
//  Created by Monika Sonawave on 10/07/2023.
//

import Foundation

protocol HomeViewModelProtocol {
    var reloadCollectionView: VoidCompletion? { get set }
    var displayError: StringCompletion? { get set }
    
    func getDogsList()
    var numberOfItems: Int { get }
    func dogName(For index: Int) -> String
    func dogImage(For index: Int) -> String
    func navigateToDetails(index: Int)
}

class HomeViewModel: HomeViewModelProtocol {
    
    // MARK: - Properties
    
    private weak var coordinator: AppCoordinatorProtocol?
    private let service: ServicesProtocol
    
    var reloadCollectionView: VoidCompletion?
    var displayError: StringCompletion?
    
    var dogs: [Dogs] = [] {
        didSet {
            reloadCollectionView?()
        }
    }
    // MARK: - Initialization
    
    init(coordinator: AppCoordinatorProtocol,
         service: ServicesProtocol = Services()) {
        self.coordinator = coordinator
        self.service = service
    }
    
    // MARK: - Networking
    
    func getDogsList() {
        service.getlist(completion: { [weak self] data, error in
            guard let self else { return }
            
            guard let data = data, error == nil else {
                self.displayError?(Constants.errorMessage)
                return
            }
            
            guard let model = try? JSONDecoder().decode([Dogs].self, from: data) else {
                self.displayError?(Constants.errorMessage)
                return
            }
            
            self.dogs = model
        })
    }
    
    //MARK: - CollectionView
    
    var numberOfItems: Int {
        dogs.count
    }
    
    func dogName(For index: Int) -> String {
        return numberOfItems > 0 ? dogs[index].name : ""
    }
    
    func dogImage(For index: Int) -> String {
        return numberOfItems > 0 ? dogs[index].image?.url ?? "" : ""
    }
    
    func navigateToDetails(index: Int) {
        coordinator?.goToDetails(details: dogs[index])
    }
}

//MARK: - Constants

extension HomeViewModel {
    struct Constants {
        static let errorMessage = "Something went wrong. Please try again"
    }
}
