//
//  Dogs.swift
//  DogsListMVVM
//
//  Created by Monika Sonawave on 12/07/2023.
//

import Foundation

struct Dogs: Codable {
    let name: String
    let id: Int?
    let image: imageDetails?
    let temperament: String?
    let life_span: String?
    let origin: String?
}

struct imageDetails: Codable {
    let url: String
}
