//
//  UICollectionView+Extension.swift
//  DogsListMVVM
//
//  Created by Monika Sonawave on 10/07/2023.
//

import UIKit

extension UICollectionView {
    
    func register<T>(_: T.Type) where T: UICollectionViewCell {
        let identifier = String(describing: T.self)
        register(T.self, forCellWithReuseIdentifier: identifier)
    }
    
    func reuse<T: UICollectionViewCell>(at indexPath: IndexPath) -> T {
        let identifier = String(describing: T.self)
        let cell = dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as? T
        return cell ?? T()
    }
}
