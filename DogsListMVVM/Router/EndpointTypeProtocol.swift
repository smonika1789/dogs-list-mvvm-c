//
//  EndpointTypeProtocol.swift
//  DogsListMVVM
//
//  Created by Monika Sonawave on 10/07/2023.
//

import Foundation

typealias HTTPHeaders = [String: String]
typealias Parameters = [String: Any]
typealias Body = [String: Any]

enum HTTPMethod: String {
    case get    = "GET"
    case post   = "POST"
    case put    = "PUT"
    case delete = "DELETE"
}

extension Parameters {
    var asString: String {
        var paramString = "?"
        for (key, value) in self {
            paramString += "\(key)=\(value)&"
        }
        paramString.removeLast()
        return paramString
    }
}

protocol EndpointTypeProtocol {
    var baseURL: URL { get }
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var headers: HTTPHeaders? { get }
    var parameters: Parameters? { get }
    var body: Body? { get }
}
