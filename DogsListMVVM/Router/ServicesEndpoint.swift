//
//  ServicesEndpoint.swift
//  DogsListMVVM
//
//  Created by Monika Sonawave on 12/07/2023.
//

import Foundation

enum ServicesEndpoint {
    case getDogsList
}

extension ServicesEndpoint: EndpointTypeProtocol {
    var baseURL: URL {
        guard let url = URL(string: "https://api.thedogapi.com/v1/") else {
            fatalError("baseURL could not be configured")
        }
        return url
    }

    var path: String {
        return "breeds"
    }
    
    var httpMethod: HTTPMethod {
        return .get
    }
    
    var headers: HTTPHeaders? { nil }
    
    var parameters: Parameters? { nil }
    
    var body: Body? { nil }
    
}
