//
//  Router.swift
//  DogsListMVVM
//
//  Created by Monika Sonawave on 10/07/2023.
//

import Foundation

typealias DataCompletion = (Data?, Error?) -> Void
typealias ResponseCompletion = ([Dogs]?, Error?) -> Void
typealias StringCompletion = (String) -> Void
typealias VoidCompletion = () -> Void

class Router {
    func requestData(_ route: EndpointTypeProtocol, completion: @escaping DataCompletion) {
        var path = route.path
        if let params = route.parameters {
            path += params.asString
        }
        guard let url = URL(string: path, relativeTo: route.baseURL) else { return }

        var request = URLRequest(url: url)
        request.httpMethod = route.httpMethod.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")

        URLSession.shared.dataTask(with: request) { data, response, error in
            if let data = data {
                completion(data, nil)
            } else if let error = error {
                completion(nil, error)
            }
        }.resume()
    }
}
