//
//  Services.swift
//  DogsListMVVM
//
//  Created by Monika Sonawave on 12/07/2023.
//

import Foundation

protocol ServicesProtocol {
    func getlist(completion: @escaping DataCompletion)
}

class Services : ServicesProtocol {
    private let router = Router()

    func getlist(completion: @escaping DataCompletion) {
        router.requestData(ServicesEndpoint.getDogsList, completion: completion)
    }
}
